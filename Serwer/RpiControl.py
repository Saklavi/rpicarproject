from socket import *
import RPi.GPIO as GPIO
import subprocess

p = subprocess.Popen(['hostname', '-I'], stdout=subprocess.PIPE, stderr=subprocess.PIPE) 
out, err = p.communicate()
GPIO.setmode(GPIO.BOARD)
s = socket(AF_INET, SOCK_STREAM)
GPIO.setup(16,GPIO.OUT)
GPIO.setup(18,GPIO.IN)
GPIO.setup(12,GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
GPIO.setup(10, GPIO.IN)
GPIO.setup(33, GPIO.OUT)

ena = GPIO.PWM(12,50)
enb = GPIO.PWM(33,150)

s.bind((out, 4444))
s.listen(5)
s, addr = s.accept()


while 1:
	print 'Polacznie z ', addr
	tm = s.recv(4096)
	print repr(tm)
	enb.start(0)
	tm = tm.decode('UTF-8').strip()
	if tm == 'przodon\x00':
		print tm
		GPIO.setup(18, GPIO.IN)
		GPIO.setup(16, GPIO.OUT)
		ena.start(90)
	
	if tm == 'przodoff\x00':
		print tm
		ena.ChangeDutyCycle(0)

	if tm == 'tylon\x00':
		print tm
		GPIO.setup(18, GPIO.OUT)
		GPIO.setup(16, GPIO.IN)
		ena.ChangeDutyCycle(90)

	if tm == 'tyloff\x00':
		print tm
		ena.ChangeDutyCycle(0)

	if tm == 'prawoon\x00':
		print tm
		GPIO.setup(10, GPIO.IN)
		GPIO.setup(22, GPIO.OUT)
		enb.ChangeDutyCycle(95)

	if tm == 'prawoff\x00':
		print tm
		enb.ChangeDutyCycle(0)

	if tm == 'lewoon\x00':
		print tm
		GPIO.setup(22, GPIO.IN)
		GPIO.setup(10, GPIO.OUT)
		enb.ChangeDutyCycle(95)

 	if tm == 'lewoff\x00':
		print tm
		enb.ChangeDutyCycle(0)
